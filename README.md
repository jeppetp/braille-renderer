# Installation

```
pip install brr
```

# Usage

```
brr --help
```

# Usage examples

Basic usage:

![r4](img/1.png)

Enable dithering:

![r2](img/2.png)

Adjust size manually:

![r1](img/3.png)

Manually set threshold:

![r3](img/4.png)
