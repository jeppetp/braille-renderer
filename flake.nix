{
  inputs = { nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable"; };

  outputs = { nixpkgs, ... }:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs { inherit system; };
      py = pkgs.python3.withPackages (pypkgs: with pypkgs; [ click pillow ]);
    in
    { packages.${system}.default = pkgs.writeShellScriptBin "brr" "${py}/bin/python ${./brr.py} $@"; };
}
